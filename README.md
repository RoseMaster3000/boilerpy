# Overview
This is a template for a python project. It aims to do the following things:
* Provide git files (`.gitattribute` and `.gitignore`)
* Provide a Pythonic `.sublime-project` file
* Provide a Readme skeleton


### Installation
If you have not already, install the following
* Install Python `paru python` or download [here](https://www.python.org/downloads/)
* Install git `paru git` or download [here](https://git-scm.com/downloads)
* Install git LFS `paru git-lfs` or download [here](https://git-lfs.github.com/)


### Git LFS
If you have a project you would like to retroactivley switch to git lfs, you can do the following
1. install large file storage `git lfs install`
2. configure a `.gitattributes` file with `*.ext filter=lfs diff=lfs merge=lfs -text`
3. activate LFS with `git lfs migrate --everything`
4. verify the `.gitattributes`
5. merge and push as needed


### Setup
1. Clone this repo `git clone <repo://url>`
3. First time setup `source setup.sh`
5. Run program `source run.sh`


### Build
Binaries can be built using [PyInstaller](https://pyinstaller.readthedocs.io/en/stable/).  
It correctly bundles PySide6 and many other popular packages.  
To install PyInstaller run `pip3 install PyInstaller`  
To compile the app.py file, run `pyinstaller app.py`  
This will create a `dist` and `build` directory.  
Your compiled application can be found in `dist`.  
The binary built by PyInstaller will target the platform you built it on.  
To cross-compile a Windows executable on Linux, refer to [this article](https://andreafortuna.org/2017/12/27/how-to-cross-compile-a-python-script-into-a-windows-executable-on-linux/
) on the topic.  


# Author
| Shahrose Kasim |             |
|----------------|-------------|
|*[shahros3@gmail.com](mailto:shahros3@gmail.com)*|[shahrose.com](http://shahrose.com)|
|*[rosemaster3000@gmail.com](mailto:rosemaster3000@gmail.com)*|[florasoft.live](https://florasoft.live) |
|*[RoseMaster#3000](https://discordapp.com/users/122224041296789508)*|[discord.com](https://discord.com/)|